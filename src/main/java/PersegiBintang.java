import java.util.Scanner;

public class PersegiBintang {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Masukkan panjang:");
        int panjang = scanner.nextInt();

        for(int i=0;i<panjang;i++){
            if(i==0 || i==panjang-1){
                for(int j=0;j<panjang;j++){
                    System.out.print("*");
                    if(j==panjang-1){
                        System.out.println();
                    }
                }
            }
            else{
                System.out.print("*");
                for(int j=0;j<panjang-2;j++){
                    System.out.print(" ");
                }
                System.out.print("*");
                System.out.println();
            }
        }
    }
}

